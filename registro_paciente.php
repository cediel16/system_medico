<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/registroA.css">
    <?php include "include/scripts.php"; ?>
    <?php include "agregar_paciente.php" ?>
    <?php include "include/header.php" ?>
    <title>Sistema de Servicios Médicos</title>
</head>

<body>
    <section id="container">
        <div class="form_register">
            <h1><i class="fas fa-user-injured"></i>+ Registro de Pacientes</h1>
            <hr>
            <div class="alert"> <?php echo isset($alert) ? $alert : ''; ?> </div>
            <form action="" method="post">
                <label for="cedula">Cedula de Identidad</label>
                <input type="text" name="cedula" id="cedula" placeholder="N° de Cédula">
                <label for="nombre">Nombre del Paciente</label>
                <input type="text" name="nombre" id="nombre" placeholder="Nombre Completo">
                <label for="apellido">Apellido del Paciente</label>
                <input type="text" name="apellido" id="apellido" placeholder="Apellido Completo">
                <label for="sexo">Sexo del Paciente</label>
                <select name="sexo" id="sexo">
                    <option value="masculino">Masculino</option>
                    <option value="femenino">Femenino</option>
                </select>
                <label for="email">Correo Electrónico</label>
                <input type="email" name="email" id="email" placeholder="Correo Electrónico">
                <label for="direccion">Dirección de Habitación</label>
                <textarea name="direccion" id="direccion" rows="05" cols="55" placeholder="Escribe aquí su dirección..."></textarea>
                <label for="telefono">Teléfono</label>
                <input type="text" name="telefono" id="telefono" placeholder="Teléfono">
                <label for="observacion">Observación</label>
                <textarea name="observacion" id="observacion" rows="05" cols="55" placeholder="Escribe aquí sus Observaciones..."></textarea>
                <section id="container2">
                
                <button type="submit" class="btn_guardar"><i class="fas fa-save"></i> Registrar</button>
                
                    <a href="lista_paciente.php" class ="btn_c"><i class="fas fa-window-close"></i> Cancelar</a>
                </section>
            </form>
        </div>
    </section>
</body>

</html>