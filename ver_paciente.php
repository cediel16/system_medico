<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/registroA.css">
    <?php include "include/scripts.php"; ?>
    <?php include "modificar_paciente.php" ?>
    <?php include "include/header.php" ?>
    <title>Sistema de Servicios Médicos</title>
</head>

<body>
    <section id="container" data-title="<?php echo $nombre . ' ' . $apellido; ?>">
        <div class="form_register">
            <h1><i class="fas fa-user-injured"></i>Datos del Paciente</h1>
            <hr>
            <div class="alert"> <?php echo isset($alert) ? $alert : ''; ?> </div>
            <form action="" method="post">
                <input type="hidden" name="idpaciente" value="<?php echo $idpaciente; ?>">
                <label for="nombre">Nombre del Paciente</label>
                <label><?php echo $nombre; ?></label>
                <br>
                <label for="apellido">Apellido del Paciente</label>
                <label><?php echo $apellido; ?></label>
                <br>
                <label for="sexo">Sexo del Paciente</label>
                <!-- <label><?php echo $option; ?></label> -->
                <br>
                <label for="email">Correo Electrónico</label>
                <label><?php echo $email; ?></label>
                <br>
                <label for="direccion">Dirección de Habitación</label>
                <label><?php echo $direccion; ?></label>
                <br>
                <label for=" telefono">Teléfono</label>
                <label><?php echo $telefono; ?></label>
                <br>
                <label for="observacion">Observación</label>
                <label><?php echo $observacion; ?></label>
                <section id="container2">

                    <a class="btn_guardar" href="editar_paciente.php?id=<?php echo $idpaciente; ?>"><i class=" fas fa-edit"></i> Editar</a>

                    <a href="lista_paciente.php" class="btn_c"><i class="fas fa-window-close"></i> Cancelar</a>
                </section>
            </form>
        </div>
    </section>
</body>

</html>