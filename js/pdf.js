function createPdf() {
    const elementoParaConvertir = document.getElementById('container'); // <-- Aquí puedes elegir cualquier elemento del DOM

    const pdf =
        html2pdf()
            .set({
                margin: 1,
                filename: elementoParaConvertir.dataset.title,
                image: {
                    type: 'jpeg',
                    quality: 0.98
                },
                html2canvas: {
                    scale: 3, // A mayor escala, mejores gráficos, pero más peso
                    letterRendering: true,
                },
                jsPDF: {
                    unit: "mm",
                    format: "letter",
                    orientation: elementoParaConvertir.dataset.orientation // landscape o portrait
                }
            })
            .from(elementoParaConvertir)
            .save()
            .catch(err => console.log(err));
}