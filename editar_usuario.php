<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/registroA.css">
    <?php include "include/scripts.php"; ?>
    <?php include "modificar_usuario.php"; ?>
    <?php include "include/header.php" ?>
    <title>Sistema de Servicios Médicos</title>
</head>

<body>
    <section id="container" data-title="<?php echo $nombre; ?>">
        <div class="form_register">
            <h1><i class="fas fa-user-plus"></i> Atualización de Usuario</h1>
            <hr>
            <div class="alert"> <?php echo isset($alert) ? $alert : ''; ?> </div>
            <form action="" method="post">
                <input type="hidden" name="idusuario" value="<?php echo $iduser; ?>">
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" id="nombre" placeholder="Nombre Completo" value="<?php echo $nombre; ?>">
                <label for="email">Correo Electrónico</label>
                <input type="email" name="email" id="email" placeholder="Correo Electrónico" value="<?php echo $correo; ?>">
                <label for="usuario">Usuario</label>
                <input type="text" name="usuario" id="usuario" placeholder="Usuario" value="<?php echo $usuario; ?>">
                <label for="password">Contraseña</label>
                <input type="password" name="password" id="password" placeholder="Clave de Acceso">
                <label for="rol">Tipo de Usuario</label>
                <select name="rol" id="rol" class="notItemOne">
                    <?php
                    echo $option;
                    ?>
                    <option value="1">Administrador</option>
                    <option value="2">Supervisor</option>
                    <option value="3">Medico</option>
                    <option value="4">Secretaria</option>
                </select>
                <section id="container2">
                    <input type="submit" value="ACTUALIZAR" class="btn_guardar">
                    <a href="lista_usuario.php" class="btn_c">Cancelar</a>
                </section>
            </form>
        </div>
    </section>
</body>

</html>