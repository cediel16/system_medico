<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/registroA.css">
    <?php include "include/scripts.php"; ?>
    <?php include "modificar_historia.php" ?>
    <?php include "include/header.php" ?>
    <title>Sistema de Servicios Médicos</title>
</head>

<body>
    <section id="container" data-title="<?php echo $nombre; ?>">
        <div class="form_register">
            <h1><i class="fas fa-notes-medical"></i> Historias Médica</h1>
            <hr>
            <div class="alert"> <?php echo isset($alert) ? $alert : ''; ?> </div>
            <form action="" method="post">
                <label for="cedula" required>Cedula de Identidad</label>
                <label><?php echo $cedula ?></label>
                <br>
                <label for="nombre">Nombre del Paciente</label>
                <label><?php echo $nombre ?></label>
                <br>
                <label for="motivo">Motivo de la Consulta</label>
                <label><?php echo $motivo ?></label>
                <br>
                <label for="observacion" required>Observación</label>
                <label><?php echo $observacion ?></label>
                <section id="container2">
                    <a class="btn_guardar" href="editar_histmedica.php?id=<?php echo $id; ?>"><i class="fas fa-edit"></i> Editar</a>
                    <a href="lista_histmedica.php" class="btn_c"><i class="fas fa-window-close"></i> Cancelar</a>
                </section>
            </form>
        </div>
    </section>
    <script>
        $(document).ready(function() {
            $("#cedula").blur(function() {
                let cedula = $('#cedula').val()
                fetch('include/consulta_paciente.php?cedula=' + cedula)
                    .then(response => response.json())
                    .then(data => {
                        if (data) {
                            let nombrecompleto = data.nombre + ' ' + data.apellido
                            $('#nombre').val(nombrecompleto)
                            document.getElementById('listo').disabled = false;
                        } else {
                            $('#nombre').val('Paciente no existe')
                            document.getElementById('listo').disabled = true;
                        }
                    });
            });
        });
    </script>

</body>

</html>