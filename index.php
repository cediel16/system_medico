<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/index.css">
    <?php include "login.php" ?>
    <title>Sistema de Registro Médico</title>
</head>

<body>
    <section id="container">
        <form action="" method="post">
            <h3>Iniciar Sesión</h3>
            <img src="img/logo_medico.png" alt="login">
            <input type="text" name="usuario" placeholder=" Ingrese su Usuario">
            <input type="password" name="password" placeholder=" Ingrese su Contraseña">
            <div class="alert"> <?php echo isset($alert) ? $alert : ''; ?> </div>
            <section id="container2">
                <input type="submit" value="INGRESAR">
            </section>
        </form>
    </section>
</body>

</html>