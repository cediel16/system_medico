<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/registroA.css">
    <?php include "include/scripts.php"; ?>
    <?php include "modificar_usuario.php"; ?>
    <?php include "include/header.php" ?>
    <title>Sistema de Servicios Médicos</title>
</head>

<body>
    <section id="container" data-title="<?php echo $nombre; ?>">
        <div class="form_register">
            <h1><i class="fas fa-user"></i> Datos del Usuario</h1>
            <hr>
            <div class="alert"> <?php echo isset($alert) ? $alert : ''; ?> </div>
            <form action="" method="post">
                <input type="hidden" name="idusuario" value="<?php echo $iduser; ?>">
                <label for="nombre">Nombre</label>
                <label><?php echo $nombre; ?></label>
                <br>
                <label for="email">Correo Electrónico</label>
                <label><?php echo $correo; ?></label>
                <br>
                <label for="usuario">Usuario</label>
                <label><?php echo $usuario; ?></label>
                <br>
                <label for="rol">Tipo de Usuario</label>
                <label><?php echo $option; ?></label>

                <section id="container2">
                    <a class="btn_guardar" href="editar_usuario.php?id=<?php echo $iduser; ?>">Editar</a>
                    <a href="lista_usuario.php" class="btn_c">Cancelar</a>
                </section>
            </form>
        </div>
    </section>
</body>

</html>