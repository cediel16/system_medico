<?php
session_start();
if (empty($_SESSION['active'])) {
    header('location:index.php');
}
?>

<?php
if (!empty($_POST)) {
    $alert = '';
    if (empty($_POST['cedula']) || empty($_POST['nombre']) || empty($_POST['apellido']) || empty($_POST['sexo']) || empty($_POST['telefono'])) {
        $alert = '<p class="msg_error">Los campos Cedula, Nombre, apellido, sexo y telefono son obligatorios.</p>';
    } else {

        include "conexion.php";

        $cedula = $_POST['cedula'];
        $nombre = $_POST['nombre'];
        $apellido = $_POST['apellido'];
        $sexo = $_POST['sexo'];
        $email = $_POST['email'];
        $direccion = $_POST['direccion'];
        $telefono = $_POST['telefono'];
        $observacion = $_POST['observacion'];

        $query = mysqli_query($conection, "SELECT * FROM paciente WHERE cedula = '$cedula'");
        $result = mysqli_fetch_array($query);

        if ($result > 0) {
            $alert = '<p class="msg_error">El Paciente ya está Registrado.</p>';
        } else {
            $query_insert = mysqli_query($conection, "INSERT INTO paciente(cedula,nombre,
                                                                            apellido,sexo,correo,direccion,
                                                                            telefono,observacion)
                                                          VALUES('$cedula','$nombre','$apellido','$sexo','$email',
                                                                    '$direccion','$telefono','$observacion')");

            if ($query_insert) {

                $alert = '<p class="msg_save">Paciente Creado Correctamente.</p>';
            } else {
                $alert = '<p class="msg_error">Error al Crear el Paciente.</p>';
            }
        }
    }
}
?>