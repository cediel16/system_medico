<?php

include "conexion.php";

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include "include/scripts.php"; ?>
    <link rel="stylesheet" href="css/estilo_tables.css">

    <title>Lista Historias Médicas</title>
</head>

<body>
    <?php include "include/header.php"; ?>
    <section id="container" data-title="Lista de historias médicas" data-orientation="landscape">

        <h1><i class="fas fa-notes-medical"></i> Lista Historias Medicas</h1>
        <a href="registro_histmedica.php" class="btn_nuevo"><i class="fas fa-notes-medical"></i>+ Crear Historia Médica</a>
        <table>
            <tr>
                <th>CODIGO HISTORIA</th>
                <th>CEDULA DE IDENTIDAD</th>
                <th>PACIENTE</th>
                <th>MOTIVO</th>
                <th>OBSERVACIONES</th>
                <th>ACCIONES</th>
            </tr>

            <?php

            $query = mysqli_query($conection, "SELECT * FROM historia INNER JOIN paciente ON historia.cedula = paciente.cedula");

            $result = mysqli_num_rows($query);
            if ($result > 0) {
                while ($data = mysqli_fetch_array($query)) {

            ?>
                    <tr>
                        <td><?php echo $data["id"]; ?></td>
                        <td><?php echo $data["cedula"]; ?></td>

                        <td><?php echo $data["nombre"]; ?> <?php echo $data["apellido"]; ?></td>
                        <td><?php echo $data["motivo"]; ?></td>
                        <td><?php echo $data["observacion"]; ?></td>
                        <td>
                            <a title="Ver historia médica" class="link_edit" href="ver_histmedica.php?id=<?php echo $data["id"]; ?>"><i class="far fa-file"></i></a>
                            <a class="separador">|</a>
                            <a title="Editar historia médica" class="link_edit" href="editar_histmedica.php?id=<?php echo $data["id"]; ?>"><i class="far fa-edit"></i></a>
                            <a class="separador">|</a>
                            <a title="Eliminar historia médica" class="link_delete" href="confirmar_delete_historia.php?id=<?php echo $data["id"]; ?>"><i class="fas fa-trash-alt"></i></a>
                        </td>
                    </tr>
            <?php
                }
            }

            ?>
        </table>



    </section>

    <?php include "include/footer.php"; ?>
</body>

</html>