<?php

include "conexion.php";

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include "include/scripts.php"; ?>
    <link rel="stylesheet" href="css/estilo_tables.css">

    <title>Lista de Pacientes</title>
</head>

<body>
    <?php include "include/header.php"; ?>
    <section id="container" data-title="Lista de pacientes" data-orientation="landscape">
        <h1><i class="fas fa-clipboard-list"></i> Lista de Pacientes</h1>
        <a href="registro_paciente.php" class="btn_nuevo"><i class="fas fa-user-injured"></i>+ Crear Paciente</a>
        <table>
            <tr>
                <th>CODIGO PACIENTE</th>
                <th>CEDULA DE IDENTIDAD</th>
                <th>NOMBRES</th>
                <th>APELLIDOS</th>
                <th>CORREO ELECTRONICO</th>
                <th>TELEFONO</th>
                <th>DIRECCION DEL PACIENTE</th>
                <th>OBSERVACIONES</th>
                <th>ACCIONES</th>
            </tr>

            <?php

            $query = mysqli_query($conection, "SELECT idpaciente,cedula, nombre, apellido, correo, 
                                                        telefono,direccion, observacion FROM paciente WHERE estatus = 1");

            $result = mysqli_num_rows($query);
            if ($result > 0) {
                while ($data = mysqli_fetch_array($query)) {

            ?>
                    <tr>
                        <td><?php echo $data["idpaciente"]; ?></td>
                        <td><?php echo $data["cedula"]; ?></td>
                        <td><?php echo $data["nombre"]; ?></td>
                        <td><?php echo $data["apellido"]; ?></td>
                        <td><?php echo $data["correo"]; ?></td>
                        <td><?php echo $data["telefono"]; ?></td>
                        <td><?php echo $data["direccion"]; ?></td>
                        <td><?php echo $data["observacion"]; ?></td>
                        <td>
                            <a title="Ver paciente" class="link_edit" href="ver_paciente.php?id=<?php echo $data["idpaciente"]; ?>"><i class="far fa-file"></i></a>
                            <a class="separador">|</a>
                            <a title="Editar paciente" class="link_edit" href="editar_paciente.php?id=<?php echo $data["idpaciente"]; ?>"><i class="far fa-edit"></i></a>
                            <a class="separador">|</a>
                            <a title="Eliminar paciente" class="link_delete" href="confirmar_delete_paciente.php?id=<?php echo $data["idpaciente"]; ?>"><i class="fas fa-trash-alt"></i></a>
                        </td>
                    </tr>
            <?php
                }
            }

            ?>
        </table>



    </section>

    <?php include "include/footer.php"; ?>
</body>

</html>