<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/registroA.css">
    <?php include "include/scripts.php"; ?>
    <?php include "modificar_historia.php" ?>
    <?php include "include/header.php" ?>
    <title>Sistema de Servicios Médicos</title>
</head>

<body>
    <section id="container">
        <div class="form_register">
            <h1><i class="fas fa-notes-medical"></i>Actualizar Historias Médica</h1>
            <hr>
            <div class="alert"> <?php echo isset($alert) ? $alert : ''; ?> </div>
            <form action="" method="post">
                <label for="cedula" required>Cedula de Identidad</label>
                <input type="number" name="cedula" id="cedula" placeholder="N° de Cédula" value="<?php echo $cedula; ?>">
                <label for="nombre">Nombre del Paciente</label>
                <input type="text" name="nombre" id="nombre" placeholder="Nombre Completo" disabled value="<?php echo $nombre; ?>">
                <label for="motivo">Motivo de la Consulta</label>
                <input type="text" name="motivo" id="motivo" placeholder="Motivo de la consulta" required value="<?php echo $motivo; ?>">
                <label for="observacion" required>Observación</label>
                <textarea name="observacion" id="observacion" rows="05" cols="50" placeholder="Escribe aquí sus Observaciones..."><?php echo $observacion; ?></textarea>
                <section id="container2">
                    <button type="submit" class="btn_guardar" id="listo"> <i class="far fa-save"></i> Registrar</button>
                    <a href="lista_histmedica.php" class="btn_c"><i class="fas fa-window-close"></i>Cancelar</a>
                </section>
            </form>
        </div>
    </section>
    <script>
        $(document).ready(function() {
            $("#cedula").blur(function() {
                let cedula = $('#cedula').val()
                fetch('include/consulta_paciente.php?cedula=' + cedula)
                    .then(response => response.json())
                    .then(data => {
                        if (data) {
                            let nombrecompleto = data.nombre + ' ' + data.apellido
                            $('#nombre').val(nombrecompleto)
                            document.getElementById('listo').disabled = false;
                        } else {
                            $('#nombre').val('Paciente no existe')
                            document.getElementById('listo').disabled = true;
                        }
                    });
            });
        });
    </script>

</body>

</html>