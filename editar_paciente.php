<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/registroA.css">
    <?php include "include/scripts.php"; ?>
    <?php include "modificar_paciente.php" ?>
    <?php include "include/header.php" ?>
    <title>Sistema de Servicios Médicos</title>
</head>

<body>
    <section id="container">
        <div class="form_register">
            <h1><i class="fas fa-user-injured"></i>+ Actualización Paciente</h1>
            <hr>
            <div class="alert"> <?php echo isset($alert) ? $alert : ''; ?> </div>
            <form action="" method="post">
                <input type="hidden" name="idpaciente" value="<?php echo $idpaciente; ?>">
                <label for="nombre">Nombre del Paciente</label>
                <input type="text" name="nombre" id="nombre" value="<?php echo $nombre; ?>">
                <label for="apellido">Apellido del Paciente</label>
                <input type="text" name="apellido" id="apellido" value="<?php echo $apellido; ?>"">
                <label for=" sexo">Sexo del Paciente</label>
                <select name="sexo" id="sexo">
                    <?php
                    echo $option;
                    ?>
                    <option value="masculino">Masculino</option>
                    <option value="femenino">Femenino</option>
                </select>
                <label for="email">Correo Electrónico</label>
                <input type="email" name="email" id="email" value="<?php echo $email; ?>">
                <label for="direccion">Dirección de Habitación</label>
                <textarea name="direccion" id="direccion" rows="05" cols="55" placeholder="Ingrese Nuevamente la Dirección..."><?php echo $direccion ?></textarea>
                <label for=" telefono">Teléfono</label>
                <input type="text" name="telefono" id="telefono" value="<?php echo $telefono; ?>">
                <label for="observacion">Observación</label>
                <textarea name="observacion" id="observacion" rows="05" cols="55" placeholder="Ingrese Nuevamente la Observacion..."><?php echo $observacion ?></textarea>
                <section id="container2">

                    <button type="submit" class="btn_guardar"><i class="fas fa-save"></i> Registrar</button>

                    <a href="lista_paciente.php" class="btn_c"><i class="fas fa-window-close"></i> Cancelar</a>
                </section>
            </form>
        </div>
    </section>
</body>

</html>