<?php

include "conexion.php";

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include "include/scripts.php"; ?>
    <link rel="stylesheet" href="css/estilo_tables.css">
    <title>Lista de Usuarios</title>
</head>

<body>
    <?php include "include/header.php"; ?>
    <section id="container" data-title="Lista de usuarios" data-orientation="landscape">
        <h1><i class="fas fa-users"></i> Lista de Usuarios</h1>
        <a href="registro_usuario.php" class="btn_nuevo"><i class="fas fa-user-plus"></i> Crear Usuarios</a>
        <table>
            <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>CORREO</th>
                <th>USUARIO</th>
                <th>ROL</th>
                <th>ACCIONES</th>
            </tr>

            <?php

            $query = mysqli_query($conection, "SELECT u.idusuario,u.nombre, u.correo, u.usuario, r.rol
                                               FROM usuario u 
                                               INNER JOIN rol r 
                                               on u.rol = r.idrol
                                               WHERE estatus = 1");

            $result = mysqli_num_rows($query);
            if ($result > 0) {
                while ($data = mysqli_fetch_array($query)) {

            ?>
                    <tr>
                        <td><?php echo $data["idusuario"]; ?></td>
                        <td><?php echo $data["nombre"]; ?></td>
                        <td><?php echo $data["correo"]; ?></td>
                        <td><?php echo $data["usuario"]; ?></td>
                        <td><?php echo $data["rol"]; ?></td>
                        <td>
                            <a title="Ver usuario" class="link_edit" href="ver_usuario.php?id=<?php echo $data["idusuario"]; ?>"><i class="fas fa-user"></i> Ver</a>
                            <a class="separador">|</a>
                            <a title="Editar usuario" class="link_edit" href="editar_usuario.php?id=<?php echo $data["idusuario"]; ?>"><i class="fas fa-user-edit"></i> Editar</a>
                            <a class="separador">|</a>
                            <?php
                            if ($data["idusuario"] != 1) {
                            ?>
                                <a title="Eliminar usuario" class="link_delete" href="confirmar_delete.php?id=<?php echo $data["idusuario"]; ?>"><i class="fas fa-trash-alt"></i> Eliminar</a>
                            <?php
                            }
                            ?>
                        </td>
                    </tr>
            <?php
                }
            }
            ?>
        </table>
    </section>

    <?php include "include/footer.php"; ?>
</body>

</html>