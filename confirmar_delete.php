<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/delete2.css">
    <?php include "include/scripts.php"; ?>
    <?php include "eliminar_usuario.php"; ?>
    <title>Eliminar Usuario</title>
</head>
<body id="body">
    <?php include "include/header.php"; ?>
       <section id="container">
        <div class="data_delete">
            
            <h2><i class="fas fa-user-times"></i>¿Estas Seguro de Eliminar  a este Usuario?</h2>
            <p>Nombre: <span><?php  echo $nombre;?></span></p>
            <p>Usuario: <span><?php  echo $usuario;?></span></p>
            <p>Tipo de Usuario: <span><?php  echo $rol;?></span></p>

            <form method="post" action="">
                <input type="hidden" name="idusuario" value="<?php echo $idusuario; ?>">
                <a href="lista_usuario.php" class ="btn_cancel">Cancelar</a>
                <input type="submit" value="Aceptar" class ="btn_ok">
            </form>
        </div>
    </section>
    <?php include "include/footer.php"; ?>
</body>
</html>