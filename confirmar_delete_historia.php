<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/delete2.css">
    <?php include "include/scripts.php"; ?>
    <?php include "eliminar_historia.php"; ?>
    <title>Eliminar Historia</title>
</head>

<body id="body">
    <?php include "include/header.php"; ?>
    <section id="container">
        <div class="data_delete">

            <h2><i class="fas fa-user-times"></i>¿Estas Seguro de Eliminar esta Historia?</h2>
            <p>Cedula: <span><?php echo $cedula; ?></span></p>
            <p>Nombre: <span><?php echo $nombre; ?></span></p>
            <p>Motivo: <span><?php echo $motivo; ?></span></p>


            <form method="post" action="">
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <a href="lista_paciente.php" class="btn_cancel">Cancelar</a>
                <input type="submit" value="Aceptar" class="btn_ok">
            </form>
        </div>
    </section>
    <?php include "include/footer.php"; ?>
</body>

</html>