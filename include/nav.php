<nav>
    <ul>
        <li><a href="menu.php"><i class="fas fa-home"></i> Inicio</a></li>
        <li class="principal">
            <a href="#"><i class="fas fa-users"></i> Usuarios</a>
            <ul>
                <li><a href="registro_usuario.php"><i class="fas fa-user-plus"></i> Nuevo Usuario</a></li>
                <li><a href="lista_usuario.php"><i class="fas fa-users"></i> Lista de Usuarios</a></li>
            </ul>
        </li>
        <li class="principal">
            <a href="#"><i class="fas fa-procedures"></i> Pacientes</a>
            <ul>
                <li><a href="registro_paciente.php"><i class="fas fa-user-injured"></i> Nuevo Paciente</a></li>
                <li><a href="lista_paciente.php"><i class="fas fa-clipboard-list"></i> Lista de Pacientes</a></li>
            </ul>
        </li>
        <li class="principal">
            <a href="#"><i class="fas fa-laptop-medical"></i> Registro Historia Médica</a>
            <ul>
                <li><a href="registro_histmedica.php"><i class="fas fa-notes-medical"></i> Nueva Historia Médica</a></li>
                <li><a href="lista_histmedica.php"><i class="fas fa-clipboard-list"></i> Lista de Historias Médicas</a></li>
            </ul>
        </li>
    </ul>
</nav>