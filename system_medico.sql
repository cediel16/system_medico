-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 24-05-2021 a las 00:03:02
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `system_medico`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historia`
--

CREATE TABLE `historia` (
  `id` int(11) NOT NULL,
  `cedula` int(11) NOT NULL,
  `motivo` varchar(300) NOT NULL,
  `observacion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `historia`
--

INSERT INTO `historia` (`id`, `cedula`, `motivo`, `observacion`) VALUES
(1, 15495958, 'Motiovo', 'paciente hipertenso.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

CREATE TABLE `paciente` (
  `idpaciente` int(15) NOT NULL,
  `cedula` int(15) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellido` varchar(30) NOT NULL,
  `sexo` varchar(12) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `direccion` text NOT NULL,
  `telefono` varchar(12) NOT NULL,
  `observacion` text NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `paciente`
--

INSERT INTO `paciente` (`idpaciente`, `cedula`, `nombre`, `apellido`, `sexo`, `correo`, `direccion`, `telefono`, `observacion`, `estatus`) VALUES
(1, 12345678, 'carmen josefina', 'perez rodriguez', 'femenina', 'carmen@hotmail.com', 'urb. la isabelica, casa 33-100, sector 1, municipio valencia, estado carabobo.', '04128526541', 'paciente hipertenso.', 1),
(2, 15495958, 'luisner', 'arrieta', 'masculino', 'larrieta@gmail.com', 'Urb. Trapichito, casa 14, manzana l80, Municipio Valencia, Estado carabobo', '04127448497', 'paciente con hipertension arterial leve', 1),
(3, 16241446, 'carmen victoria', 'ascune guerrero', 'femenino', 'victoria@hotmail.com', 'sector pirital, casa 25-80, av. principal, municipio los guayos, Estado carabobo.', '04127444339', 'problemas leves de circulacion arterial.', 1),
(4, 15495959, 'LUIS JOSE', 'GONZALEZ', 'masculino', 'luisjose@hotmail.com', 'sector central tacarigua, al frente de la GNB', '04121234567', 'Ninguna', 1),
(5, 300, '300', '300', 'masculino', '300@300.300', '300', '300', '300', 0),
(6, 400, '400', '400', 'masculino', '400@400.400', '400', '400', '400', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `idrol` int(11) NOT NULL,
  `rol` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`idrol`, `rol`) VALUES
(1, 'Administrador'),
(2, 'Supervisor'),
(3, 'Medico'),
(4, 'Secretaria');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `usuario` varchar(15) NOT NULL,
  `clave` varchar(100) NOT NULL,
  `rol` int(11) NOT NULL,
  `estatus` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `nombre`, `correo`, `usuario`, `clave`, `rol`, `estatus`) VALUES
(1, 'Luis Arrieta', 'Larrieta@gmail.com', 'larrieta', '202cb962ac59075b964b07152d234b70', 1, 1),
(2, 'carmen rangel', 'carmenrangel@gmail.com', 'crangel', '202cb962ac59075b964b07152d234b70', 4, 1),
(3, 'jose luis marquez', 'joseluismarquez@gmail.com', 'jmarquez', '202cb962ac59075b964b07152d234b70', 2, 1),
(5, 'daniel chavez', 'daniel@gmail.com', 'dchavez20', '202cb962ac59075b964b07152d234b70', 1, 1),
(7, 'edrick rodriguez', 'edrick@hotmail.com', 'erodriguez', '202cb962ac59075b964b07152d234b70', 2, 1),
(8, 'Erick Rodriguez', 'erick25@hotmail.com', 'Erodriguez25', '202cb962ac59075b964b07152d234b70', 3, 1),
(9, 'Maria Rodriguez', 'maria@gmail.com', 'mrodriguez', '202cb962ac59075b964b07152d234b70', 4, 1),
(10, 'ALEXIS COLINA', 'alexiscolina@gmail.com', 'acolina', '202cb962ac59075b964b07152d234b70', 2, 1),
(11, 'Ana Karina Rodriguez', 'anakarina@gmail.com', 'akarina', '202cb962ac59075b964b07152d234b70', 2, 1),
(12, 'Lisbeth Medina sanchez', 'lisbethmedina80@gmail.com', 'lsanchez10', '202cb962ac59075b964b07152d234b70', 2, 1),
(13, 'Jhoel Rivera', 'joel@gmail.com', 'jrivera12', '202cb962ac59075b964b07152d234b70', 2, 1),
(14, 'marcos medina', 'marcos@hotmail.com', 'medina12', '202cb962ac59075b964b07152d234b70', 2, 1),
(15, 'JOSE TOVAR', 'josetovar@gmail.com', 'jtovar', '202cb962ac59075b964b07152d234b70', 2, 1),
(16, 'Jose luis Perez', 'joseperez@hotmail.com', 'jperez01', '202cb962ac59075b964b07152d234b70', 2, 1),
(17, 'Maria Coronado', 'mariacoronado25@hotmail.com', 'mcoronado10', '202cb962ac59075b964b07152d234b70', 4, 1),
(18, 'Erick Rodriguez mota', 'mota@gmail.com', 'mota12', '202cb962ac59075b964b07152d234b70', 1, 0),
(19, 'Elianni Rivas', 'eli23@gmail.com', 'erivas10', '202cb962ac59075b964b07152d234b70', 3, 1),
(20, 'Jose Peralta', 'peralta@gmail.com', 'jperalta10', '202cb962ac59075b964b07152d234b70', 1, 1),
(21, 'Maria Robertiz', 'robertiz@gmail.com', 'robertiz20', '202cb962ac59075b964b07152d234b70', 2, 1),
(22, 'Alexander chino', 'alexchino@gmail.com', 'chino', '202cb962ac59075b964b07152d234b70', 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `historia`
--
ALTER TABLE `historia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD PRIMARY KEY (`idpaciente`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`idrol`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`),
  ADD KEY `rol` (`rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `historia`
--
ALTER TABLE `historia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `paciente`
--
ALTER TABLE `paciente`
  MODIFY `idpaciente` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `idrol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `rol_usuario` FOREIGN KEY (`rol`) REFERENCES `rol` (`idrol`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
